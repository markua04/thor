import Vue from "vue";
import Router from "vue-router";
import Index from "./views/Index.vue";
import Landing from "./views/Landing.vue";
import Login from "./views/Login.vue";
import Profile from "./views/Profile.vue";
import MainNavbar from "./layout/MainNavbar.vue";
import MainFooter from "./layout/MainFooter.vue";
import Register from "./views/Register";
import FollowerRegistration from "./views/FollowerRegistration";
import Streamers from "./views/Streamers/Streamers";
import EditProfile from "./views/User/EditProfile";
import StreamerProfile from "./views/User/StreamerProfile";
import About from "./views/About";
import store from "./store";
import StreamerDashboard from "./views/User/StreamerDashboard";
import Gamers from "./views/Gamers/Gamers";
import FollowerDashboard from "./views/User/FollowerDashboard";
import ResetPassword from "./views/ResetPassword";
import PasswordResetForm from "./views/PasswordResetForm";
import Contact from "./views/Contact";
import CompleteRegistration from "./views/CompleteRegistration";
import StreamersFeed from "./views/StreamersFeed";
import Guidelines from "./views/Guidelines";

Vue.use(Router);

let router = new Router({
  routes: [
    {
      path: "/",
      name: "index",
      components: { default: Index, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/edit",
      name: "edit",
      components: {
        default: EditProfile,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/landing",
      name: "landing",
      components: { default: Landing, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/register-streamer",
      name: "register-streamer",
      components: { default: Register, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/register-follower",
      name: "register-follower",
      components: {
        default: FollowerRegistration,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/login",
      name: "login",
      components: { default: Login, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/password-reset",
      name: "password-reset",
      components: {
        default: ResetPassword,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/new-password-reset",
      name: "new-password-reset",
      components: {
        default: PasswordResetForm,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 }
      }
    },
    {
      path: "/profile",
      name: "profile",
      components: { default: Profile, header: MainNavbar, footer: MainFooter },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/streamers",
      name: "streamers",
      components: {
        default: Streamers,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/streamer-profile",
      name: "streamer-profile",
      components: {
        default: StreamerProfile,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/about",
      name: "about",
      components: {
        default: About,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/feed",
      name: "feed",
      components: {
        default: StreamersFeed,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/dashboard",
      name: "dashboard",
      components: {
        default: StreamerDashboard,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/follower-dashboard",
      name: "follower-dashboard",
      components: {
        default: FollowerDashboard,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      },
      meta: {
        requiresAuth: true
      }
    },
    {
      path: "/gamers",
      name: "gamers",
      components: {
        default: Gamers,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/contact",
      name: "contact",
      components: {
        default: Contact,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/complete-registration",
      name: "complete-registration",
      components: {
        default: CompleteRegistration,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    },
    {
      path: "/guidelines",
      name: "guidelines",
      components: {
        default: Guidelines,
        header: MainNavbar,
        footer: MainFooter
      },
      props: {
        header: { colorOnScroll: 400 },
        footer: { backgroundColor: "black" }
      }
    }
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.state.status === "success") {
      next();
    } else {
      next(router.push({ path: "/login" }));
    }
  } else {
    next();
  }
});

export default router;
