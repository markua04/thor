import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";
import Users from "./services/api/Users";
import router from "./router";
import { odinUrl } from "./main";
import VuexPersist from "vuex-persist";

const vuexPersist = new VuexPersist({
  key: "stream-find",
  storage: window.localStorage
});

Vue.use(Vuex);

export default new Vuex.Store({
  plugins: [vuexPersist.plugin],
  state: {
    status: "",
    token: "",
    user: "",
    userType: "",
    follows: "",
    notifications: ""
  },
  mutations: {
    auth_request(state) {
      state.status = "loading";
    },
    auth_success(state, data) {
      state.status = "success";
      state.token = data.token;
      state.user = data.user;
      state.follows = JSON.parse(JSON.stringify(data.follows));
      state.notifications = 0;
      state.notificationData = data.notificationData;
      state.gamingPlatforms = data.gamingPlatforms;
      if (data.user.user_type === 2) {
        state.userType = "streamer";
      } else {
        state.userType = "follower";
      }
    },
    auth_update(state, user) {
      state.status = "success";
      state.token = user.auth_token;
      state.user = user;
      if (user.user_type === 2) {
        state.userType = "streamer";
      } else {
        state.userType = "follower";
      }
    },
    auth_error(state) {
      state.status = "error";
    },
    logout(state) {
      state.status = "";
      state.token = "";
      state.user = "";
      state.userType = "";
      state.follows = "";
      state.notifications = 0;
      state.notificationData = "";
      state.gamingPlatforms = "";
    },
    update_follows(state, follows) {
      state.follows = follows;
    },
    update_notification_count(state, notifications) {
      state.notifications = notifications;
    },
    update_notification_data(state, notificationData) {
      state.notificationData = notificationData;
    }
  },
  actions: {
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios({
          url: odinUrl + "/api/v1/login",
          data: user,
          method: "POST"
        })
          .then(response => {
            console.log(response);
            const token = response.data.token.token;
            const user = response.data.user;
            const follows = response.data.userFollows;
            const notifications = response.data.userNotifications;
            const gamingPlatforms = response.data.gamingPlatforms;
            const notificationCount = notifications.length;
            let userType = "follower";
            if (response.data.user.user_type === 2) {
              userType = "streamer";
            }
            axios.defaults.headers.common["Authorization"] = "Bearer " + token;
            commit("auth_success", {
              token: token,
              user: user,
              userType: userType,
              follows: follows,
              notifications: notificationCount,
              notificationData: notifications,
              gamingPlatforms: gamingPlatforms
            });
            router.push({
              name: "edit",
              params: { user: user }
            });
          })
          .catch(err => {
            commit("auth_error");
            reject(err);
            router.push({
              name: "login",
              params: { user: user }
            });
            return err;
          });
      });
    },
    register({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios
          .post(odinUrl + "/api/v1/register", {
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
          })
          .then(response => {
            let message =
              "WE SENT YOU AN EMAIL WHEN YOU REGISTERED. PLEASE OPEN THAT EMAIL AND CLICK THE ACTIVATE ACCOUNT LINK.";
            if (response.data === "ER_DUP_ENTRY") {
              message = "You have already registered. Please login here!";
            } else {
              message =
                "You have successfully registered! Please check your email and confirm your registration.";
            }
            router.push({
              name: "login",
              params: {
                showMessage: true,
                message: message
              }
            });
          })
          .catch(err => {
            let responseData = err.response.data;
            commit("auth_error");
            reject(err);
            return responseData;
          });
      });
    },
    registerFollower({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit("auth_request");
        axios
          .post(odinUrl + "/api/v1/register-follower", {
            headers: {
              "Content-Type": "application/json"
            },
            body: JSON.stringify(user)
          })
          .then(response => {
            let message = "";
            if (response.data === "ER_DUP_ENTRY") {
              message = "You have already registered. Please login here!";
            } else if (response.data === "ER_BAD_FIELD_ERROR") {
              message =
                "Something has gone wrong! Please contact an administrator to assist you.";
            } else {
              message =
                "You have successfully registered! Please check your email and confirm your registration.";
            }
            router.push({
              name: "login",
              params: {
                showMessage: true,
                message: message
              }
            });
          })
          .catch(err => {
            let responseData = err.response.data;
            commit("auth_error");
            reject(err);
            return responseData;
          });
      });
    },
    logout({ commit }) {
      return new Promise(resolve => {
        commit("logout");
        resolve();
        Users.logoutUser().then(response => {
          if (this.$ws) {
            const topicName = "status";
            this.$ws.$off(`${topicName}|live`);
            this.$ws.$off(`${topicName}|update`);
          }
        });
      });
    },
    updateFollows({ commit }, follows) {
      commit("updateFollows", { follows: follows });
    },
    updateNotifications({ commit }, notifications) {
      commit("updateNotifications", { notifications: notifications });
    }
  },
  getters: {}
});
