import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import axios from "axios";
import MaterialKit from "./plugins/material-kit";
import LoadScript from "vue-plugin-load-script";
Vue.config.productionTip = false;
import store from "./store";
import configUrls from "./config";
import VModal from "vue-js-modal";
import WsPlugin from "adonis-vue-websocket";
import { Datetime } from "vue-datetime";
// You need a specific loader for CSS files
import "vue-datetime/dist/vue-datetime.css";
import VueNoty from "vuejs-noty";
import vSelect from "vue-select";

import "vue2-timepicker/dist/VueTimepicker.css";

// eslint-disable-next-line no-unused-vars
export const odinUrl = configUrls.API_URL;
export const wsUrl = configUrls.WS_URL;

Vue.use(MaterialKit);
Vue.use(LoadScript);
Vue.use(VModal);
Vue.use(WsPlugin, { adonisWS: window.adonis.Ws });
Vue.use(require("vue-moment"));
Vue.use(Datetime);
Vue.use(VueNoty);
Vue.component("v-select", vSelect);

export const NavbarStore = {
  showNavbar: false
};

Vue.mixin({
  data() {
    return {
      NavbarStore,
      odinUrl,
      wsUrl
    };
  }
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
axios.defaults.headers.common["Authorization"] = "Bearer ".concat(
  store.state.token
);
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["Accept"] = "application/json";
