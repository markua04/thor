import axios from "axios/index";
import config from "../../config";

// const headers = {
//   "Content-Type": "application/json"
// };

export default {
  getAllGames(query) {
    return axios
      .get(config.API_URL + "/api/v1/get-games?query=" + query)
      .then(response => {
        return response.data;
      })
      .catch(error => {
        return console.log("error " + error);
      });
  }
};
