import router from "../../router";

export default {
  errorHandler(error) {
    if (error.response.data.includes("E_JWT_TOKEN_EXPIRED")) {
      this.logout();
      router.push({
        name: "login",
        params: {
          messages: "Your session has expired. Please login to continue!"
        },
        props: true
      });
    }
    if (error.response.data.error.name === "InvalidJwtToken") {
      this.logout();
      router.push({
        name: "login",
        params: {
          messages: "Your session has expired. Please login to continue!"
        },
        props: true
      });
    }
    if (error.response.data.error.name === "ExpiredJwtToken") {
      this.logout();
      router.push({
        name: "index",
        params: {
          messages: "Your session has expired. Please login to continue!"
        },
        props: true
      });
    }

    if (error.response.data.error && error.response.data.error.name) {
      alert(
        "Something has gone wrong. Please contact an administrator on the contact page."
      );
    }
  }
};
