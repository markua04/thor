import axios from "axios/index";
import store from "../../store";
import router from "../../router";
import ErrorHandler from "./ErrorHandler";

const headers = {
  "Content-Type": "application/json;charset=UTF-8"
};

export default {
  async apiGet(url) {
    if (store.state.status !== "") {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + store.state.token;
    }
    return axios
      .get(url, {
        headers: headers
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        if (error.response.status === 200) {
          return error.response;
        } else if (error.response.status === 422) {
          return error.response;
        } else if (error.response.status === 401) {
          store
            .dispatch("logout")
            .then(() => {
              router.push({ path: "/login?expired_token=true" });
            })
            .catch(err => console.log(err));
          //error.response.data to get string
        } else {
          ErrorHandler.errorHandler();
        }
      });
  },
  apiPost(url, data) {
    if (store.state.status !== "") {
      axios.defaults.headers.common["Authorization"] =
        "Bearer " + store.state.token;
    }
    return axios
      .post(url, {
        headers: headers,
        body: data
      })
      .then(response => {
        return response;
      })
      .catch(error => {
        if (error.response.status === 200) {
          return error.response;
        } else if (error.response.status === 422) {
          return error.response;
        } else if (error.response.status === 401) {
          store
            .dispatch("logout")
            .then(response => {
              router.push({
                name: "/login"
              });
            })
            .catch(err => console.log(err));
        } else {
          ErrorHandler.errorHandler();
        }
      });
  },
  logout: function() {
    return store.dispatch("logout").catch(err => console.log(err));
  },
  errorMessage(message) {
    router.push({
      name: "index",
      params: { messages: message },
      props: true
    });
  }
};
