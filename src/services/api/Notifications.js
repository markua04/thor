import ApiService from "./ApiService";
import configUrls from "../../config";

export default {
  async getUsersNotifications(following) {
    let url = configUrls.API_URL + "/api/v1/notifications";
    return await ApiService.apiPost(url, following).then(response => {
      console.log(response.data);
      return response.data;
    });
  }
};
