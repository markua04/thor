import axios from "axios";
import config from "../../config";
import Contacts from "./Contacts";

export default {
  async getRemoteIp() {
    return await Contacts.getUsersIp();
  },
  async validate(response) {
    let params = {
      response: response
    };
    return axios
      .post(config.API_URL + "/api/v1/recaptcha/validate", params)
      .then(response => {
        return response;
      })
      .catch(error => {
        return error;
      });
  }
};
