import axios from "axios/index";
import store from "../../store";
import ApiService from "./ApiService";
import configUrls from "../../config";

export default {
  async contact(data) {
    if (store.state.status === "") {
      axios.defaults.headers.common["Authorization"] = "";
    }
    let usersIp = await this.getUsersIp();
    if (usersIp !== null || typeof usersIp !== "undefined") {
      data.users_ip = usersIp;
      let url = configUrls.API_URL + "/api/v1/contact";
      return ApiService.apiPost(url, data).then(response => {
        return response;
      });
    }
  },
  async getUsersIp() {
    let url = configUrls.API_URL + "/api/v1/get-users-ip";
    return await ApiService.apiGet(url).then(response => {
      return response.data.ip;
    });
  }
};
