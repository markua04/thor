import axios from "axios/index";
import store from "../../store";
import configUrls from "../../config";
import ApiService from "./ApiService";
import {odinUrl} from "../../main";

export default {
  getUser(name, id) {
    let url =
      configUrls.API_URL +
      "/api/v1/streamer-profile?name=" +
      name +
      "&user=" +
      id;
    return ApiService.apiGet(url);
  },
  getStreamers(data) {
    if (store.state.status === "") {
      axios.defaults.headers.common["Authorization"] = "";
    }
    let url = configUrls.API_URL + "/api/v1/search";
    return ApiService.apiPost(url, data).then(response => {
      return response.data.users;
    });
  },
  getGamers(data) {
    let url = configUrls.API_URL + "/api/v1/search-gamers";
    let body = JSON.stringify(data);
    return ApiService.apiPost(url, body).then(response => {
      return response.data.users;
    });
  },
  registerUser(user) {
    let url = configUrls.API_URL + "/api/v1/register";
    let body = JSON.stringify(user);
    return ApiService.apiPost(url, body).then(response => {
      return response.data;
    });
  },
  logoutUser() {
    let url = configUrls.API_URL + "/api/v1/logout";
    let body = JSON.stringify(store.state.token);
    return ApiService.apiPost(url, body).then(response => {
      if (response.status === 200) {
        localStorage.setItem("token", "");
        window.localStorage.clear();
        delete axios.defaults.headers.common["Authorization"];
      }
    });
  },
  getUserByToken() {
    let url = configUrls.API_URL + "/api/v1/get-user-by-token";
    return ApiService.apiGet(url);
  },
  updateUser(user) {
    let url = configUrls.API_URL + "/api/v1/user/edit";
    let body = JSON.stringify(user);
    return ApiService.apiPost(url, body);
  },
  redirectHome(router) {
    router.push({
      name: "index"
    });
  },
  getLatestStreamers(userCountryCode) {
    let url =
      configUrls.API_URL +
      "/api/v1/get-latest-streamers?count=8&code=" +
      userCountryCode;
    return ApiService.apiGet(url);
  },
  getUserDash() {
    if (store.state.user.user_type === 2) {
      let url = configUrls.API_URL + "/api/v1/get-user-dash";
      return ApiService.apiGet(url, this.$router);
    } else {
      let url = configUrls.API_URL + "/api/v1/get-follower-dash";
      return ApiService.apiGet(url, this.$router);
    }
  },
  followStreamer(gamer_tag) {
    let url =
      configUrls.API_URL + "/api/v1/follow-streamer?gamer_tag=" + gamer_tag;
    return ApiService.apiGet(url);
  },
  unfollowStreamer(gamer_tag) {
    let url =
      configUrls.API_URL + "/api/v1/unfollow-streamer?gamer_tag=" + gamer_tag;
    return ApiService.apiGet(url);
  },
  resetPassword(email) {
    let userEmail = { email: email };
    let url = configUrls.API_URL + "/api/v1/password-reset";
    return ApiService.apiPost(url, userEmail);
  },
  newPasswordReset(data) {
    let url = configUrls.API_URL + "/api/v1/new-password-reset";
    return ApiService.apiPost(url, data);
  },
  async twitterBearerToken() {
    let url = configUrls.API_URL + "/api/v1/twitter-bearer";
    return await ApiService.apiGet(url);
  },
  twitterFollowers() {
    let url =
      "https://api.twitter.com/1.1/users/show.json?screen_name=twitterdev";
    const headers = {
      Authorization: "OAuth oauth_consumer_key=H8WLRfDOyMjNtR6KQjK4rHwwr"
    };
    return axios
      .get(url, {
        headers: headers
      })
      .then(response => {
        console.log(response);
        return response.data;
      })
      .catch(error => {
        return console.log("error " + error);
      });
  },
  completeRegistration(code) {
    let url = configUrls.API_URL + "/api/v1/complete-registration?code=" + code;
    return ApiService.apiGet(url);
  },
  getLatestFeed(data) {
    let url = configUrls.API_URL + "/api/v1/feed";
    return ApiService.apiPost(url, data);
  },
  reportProfile(data) {
    let url = configUrls.API_URL + "/api/v1/report-profile";
    return ApiService.apiPost(url, data);
  },
  getGamesPlatforms() {
    let url = configUrls.API_URL + "/api/v1/get-games-platforms";
    return ApiService.apiGet(url);
  },
  updateProfilePic(data) {
    const url = configUrls.API_URL + "/api/v1/user-image-upload";
    return ApiService.apiPost(url, data);
  },
};
