import axios from "axios/index";

// const odinUrl = "http://api.streamfind.me";
axios.defaults.headers.common["Access-Control-Allow-Origin"] = "*";
const twitchUrl = "https://api.twitch.tv";
const twitchAuthUrl = "https://id.twitch.tv";
const headers = {
  "Content-Type": "application/json",
  "Client-ID": "o5dl06kk60ixle061k1jv23nszqv4y"
};

export default {
  getTwitchUser() {
    this.getTwitchToken().then(response => {
      const customHeaders = {
        "Content-Type": "application/json",
        "Client-ID": "o5dl06kk60ixle061k1jv23nszqv4y",
        Authorization: "Bearer"
      };
      return axios
        .get(twitchUrl + "/helix/streams?game_id=33214", {
          headers: customHeaders
        })
        .then(response => {
          console.log(response.data);
          return response.data;
        });
    });
  },
  getTwitchToken() {
    const customHeaders = {
      "Content-Type": "application/json",
      "Client-ID": "o5dl06kk60ixle061k1jv23nszqv4y"
    };
    return axios
      .get(
        twitchAuthUrl +
          "/oauth2/authorize?client_id=o5dl06kk60ixle061k1jv23nszqv4y&response_type=code&scope=channel_read",
        {
          headers: customHeaders
        }
      )
      .then(response => {
        console.log(response.data);
        return response.data;
      });
  }
};
