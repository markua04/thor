import countryRegionData from "country-region-data";
export default {
  getCountries() {
    var arrayOfObjects = [];
    for (let i = 0; i < countryRegionData.length; i++) {
      var obj = {};
      obj = countryRegionData[i].countryName;
      arrayOfObjects.push(obj);
    }
    return arrayOfObjects;
  },
  getCountryCodeByName(country) {
    let result;
    for (let i = 0, len = countryRegionData.length; i < len; i++) {
      if (countryRegionData[i].countryName === country) {
        result = countryRegionData[i].countryShortCode;
        break;
      }
    }
    return result;
  },
  getCountryNameByCode(code) {
    let result;
    for (let i = 0, len = countryRegionData.length; i < len; i++) {
      if (countryRegionData[i].countryShortCode === code) {
        result = countryRegionData[i].countryName;
        break;
      }
    }
    return result;
  },
  buildCountriesData(countryRegionData) {
    let countries = countryRegionData;
    let countCountries = countries.length;
    let countriesData = [];
    for (let i = 0; i < countCountries; i++) {
      if (i === 0) {
        countriesData[i] = {
          id: "",
          name: "None"
        };
      } else {
        countriesData[i] = {
          id: countries[i].countryShortCode,
          name: countries[i].countryName
        };
      }
    }
    return countriesData;
  }
};
