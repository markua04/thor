export default {
  getUserTypes() {
    return ["Follower", "Streamer"];
  },
  getUserTypeByName(typeName) {
    if (typeName === "Follower") {
      return 3;
    }
    return 2;
  }
};
